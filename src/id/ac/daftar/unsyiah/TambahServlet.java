package id.ac.daftar.unsyiah;



import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

@SuppressWarnings("serial")
public class TambahServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException 
	{
		resp.setContentType("text/plain");
		RequestDispatcher jsp = req.getRequestDispatcher("tambah.jsp");
		jsp.forward(req, resp);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException 
	{
		String Nama = req.getParameter("txtNama");
		String Nim = req.getParameter("txtNim");
		String Email = req.getParameter("txtEmail");
		Long NoHp = Long.valueOf(req.getParameter("txtNoHp"));
		String Aktif = req.getParameter("status");
		
		
		Entity entity = new Entity("Mahasiswa");
		entity.setProperty("Nama", Nama);
		entity.setProperty("Nim", Nim);
		entity.setProperty("Email", Email);
		entity.setProperty("NoHp", NoHp);
		entity.setProperty("Aktif", Aktif);
		
		DatastoreService datastoreService = DatastoreServiceFactory.getDatastoreService();
		datastoreService.put(entity);
		
		resp.sendRedirect("/");
	}
}
